Source: process-cpp
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 doxygen,
 graphviz,
 libboost-dev,
 libboost-iostreams-dev,
 libboost-system-dev,
 libproperties-cpp-dev,
 pkg-config,
 rdfind,
 symlinks,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Section: libs
Homepage: https://gitlab.com/ubports/development/core/lib-cpp/process-cpp
Vcs-Git: https://salsa.debian.org/ubports-team/process-cpp.git
Vcs-Browser: https://salsa.debian.org/ubports-team/process-cpp

Package: libprocess-cpp3
Architecture: linux-any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: C++11 library for handling processes (runtime libraries)
 process-cpp is a simple and straightforward wrapper around process creation
 and control. It helps both with handling child processes and with interacting
 with the current process. Some of its features include:
 .
  - Thread-safe get/set/unset operation on the current process's environment.
  - Throwing and non-throwing overloads of functions when system calls are
    involved.
  - Seamless redirection of input, output and error streams of child processes.
  - Type-safe interaction with the virtual proc filesystem, both for reading &
    writing.
 .
 The library's main purpose is to assist in testing and when a software
 component needs to carry out process creation/control tasks, e.g., a graphical
 shell. To this end, the library is extensively tested and tries to ensure
 fail-safe operation as much as possible.
 .
 This package includes the process-cpp runtime libraries.

Package: libprocess-cpp-dev
Section: libdevel
Architecture: linux-any
Multi-Arch: same
Recommends:
 libprocess-cpp-doc
Depends:
 libprocess-cpp3 (= ${binary:Version}),
 libproperties-cpp-dev,
 ${misc:Depends}
Description: C++11 library for handling processes (dev headers and libraries)
 process-cpp is a simple and straightforward wrapper around process creation
 and control. It helps both with handling child processes and with interacting
 with the current process. Some of its features include:
 .
  - Thread-safe get/set/unset operation on the current process's environment.
  - Throwing and non-throwing overloads of functions when system calls are
    involved.
  - Seamless redirection of input, output and error streams of child processes.
  - Type-safe interaction with the virtual proc filesystem, both for reading &
    writing.
 .
 This package includes all the development headers and libraries for
 process-cpp.

Package: libprocess-cpp-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends}
Suggests:
 libprocess-cpp-dev
Description: C++11 library for handling processes (documentation)
 process-cpp is a simple and straightforward wrapper around process creation
 and control. It helps both with handling child processes and with interacting
 with the current process. Some of its features include:
 .
  - Thread-safe get/set/unset operation on the current process's environment.
  - Throwing and non-throwing overloads of functions when system calls are
    involved.
  - Seamless redirection of input, output and error streams of child processes.
  - Type-safe interaction with the virtual proc filesystem, both for reading &
    writing.
 .
 This package includes documentation files for the libprocess-cpp development.
